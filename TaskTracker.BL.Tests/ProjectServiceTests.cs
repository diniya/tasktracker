﻿using System.ComponentModel.DataAnnotations;
using NUnit.Framework;
using TaskTracker.BL.Models;
using TaskTracker.BL.Services;
using TaskTracker.DAL.Entities;

namespace TaskTracker.BL.Tests
{
    public class ProjectServiceTests
    {
        [Test]
        public void Validate_ShouldNot_HaveErrors()
        {
            var model = new ProjectModel()
            {
                Name = "Project 1",
                Status = StatusProject.Active,
                Priority = 1
            };
            ProjectService.ValidateProjectModel(model);
        }
        
        [Test]
        public void Validate_Should_HaveErrors_WhenNameIsNull()
        {
            var model = new ProjectModel()
            {
                Name = null,
                Status = StatusProject.Active,
                Priority = 1
            };
            Assert.Throws<ValidationException>(() =>
            {
                ProjectService.ValidateProjectModel(model);
            });
        }
        
        [Test]
        public void Validate_Should_HaveErrors_WhenNameIsEmpty()
        {
            var model = new ProjectModel()
            {
                Name = "",
                Status = StatusProject.Active,
                Priority = 1
            };
            Assert.Throws<ValidationException>(() =>
            {
                ProjectService.ValidateProjectModel(model);
            });
        }
        
        [Test]
        public void Validate_Should_HaveErrors_WhenStatusIsNull()
        {
            var model = new ProjectModel()
            {
                Name = "Project 1",
                Status = (StatusProject)3,
                Priority = 1
            };
            Assert.Throws<ValidationException>(() =>
            {
                ProjectService.ValidateProjectModel(model);
            });
        }
        
        [Test]
        public void Validate_Should_HaveErrors_WhenPriorityLessThanOne()
        {
            var model = new ProjectModel()
            {
                Name = "Project 1",
                Status = StatusProject.Active,
                Priority = 0
            };
            Assert.Throws<ValidationException>(() =>
            {
                ProjectService.ValidateProjectModel(model);
            });
        }
        
        [Test]
        public void Validate_Should_HaveErrors_WhenPriorityGreaterThanFive()
        {
            var model = new ProjectModel()
            {
                Name = "Project 1",
                Status = StatusProject.Active,
                Priority = 6
            };
            Assert.Throws<ValidationException>(() =>
            {
                ProjectService.ValidateProjectModel(model);
            });
        }
    }
}