﻿using System;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Moq;
using NUnit.Framework;
using TaskTracker.BL.Models;
using TaskTracker.BL.Services;
using TaskTracker.DAL.Entities;
using TaskTracker.DAL.Repository;

namespace TaskTracker.BL.Tests
{
    public class TaskServiceTests
    {
        private TasksService _service;
        private Mock<ITaskRepository> _taskRepository = new();
        private Mock<IProjectRepository> _projectRepository = new();
        private Mock<IMapper> _mapper = new();

        [SetUp]
        public void Setup()
        {
            _taskRepository = new Mock<ITaskRepository>();
            _projectRepository = new Mock<IProjectRepository>();
            _mapper = new Mock<IMapper>();
            _service = new TasksService(_taskRepository.Object, _projectRepository.Object, _mapper.Object);
        }
        
        [Test]
        public void Validate_ShouldNot_HaveErrors()
        {
            var projectId = Guid.NewGuid();
            var model = new TaskModel()
            {
                Name = "Task 1",
                Status = StatusTask.Done,
                Priority = 1,
                Description = "Description 1",
                ProjectId = projectId
            };
            _projectRepository.Setup(x => x.GetProject(projectId))
                .ReturnsAsync(new ProjectEntity());
            Assert.DoesNotThrowAsync(async () =>
            {
                await _service.ValidateTaskModel(model);
            });
        }
        
        [Test]
        public void Validate_Should_HaveErrors_WhenNoProjectById()
        {
            var projectId = Guid.NewGuid();
            var model = new TaskModel
            {
                Name = "Task 1",
                Status = StatusTask.Done,
                Priority = 1,
                Description = "Description 1",
                ProjectId = projectId
            };
            Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _service.ValidateTaskModel(model);
            });
        }
        
        [Test]
        public void Validate_Should_HaveErrors_WhenNameIsNull()
        {
            var projectId = Guid.NewGuid();
            var model = new TaskModel
            {
                Name = null,
                Status = StatusTask.Done,
                Priority = 1,
                Description = "Description 1",
                ProjectId = projectId
            };
            Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _service.ValidateTaskModel(model);
            });
        }
        
        [Test]
        public void Validate_Should_HaveErrors_WhenNameIsEmpty()
        {
            var projectId = Guid.NewGuid();
            var model = new TaskModel
            {
                Name = "",
                Status = StatusTask.Done,
                Priority = 1,
                Description = "Description 1",
                ProjectId = projectId
            };
            Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _service.ValidateTaskModel(model);
            });
        }
        
        [Test]
        public void Validate_Should_HaveErrors_WhenStatusIsIncorrect()
        {
            var projectId = Guid.NewGuid();
            var model = new TaskModel
            {
                Name = "Task 1",
                Status = (StatusTask)3,
                Priority = 1,
                Description = "Description 1",
                ProjectId = projectId
            };
            Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _service.ValidateTaskModel(model);
            });
        }
        
               
        [Test]
        public void Validate_Should_HaveErrors_WhenPriorityLessThanOne()
        {
            var projectId = Guid.NewGuid();
            var model = new TaskModel
            {
                Name = "Task 1",
                Status = StatusTask.Done,
                Priority = 0,
                Description = "Description 1",
                ProjectId = projectId
            };
            Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _service.ValidateTaskModel(model);
            });
        }
        
        [Test]
        public void Validate_Should_HaveErrors_WhenPriorityGreaterThanFive()
        {
            var projectId = Guid.NewGuid();
            var model = new TaskModel
            {
                Name = "Task 1",
                Status = StatusTask.Done,
                Priority = 6,
                Description = "Description 1",
                ProjectId = projectId
            };
            Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _service.ValidateTaskModel(model);
            });
        }
        
        [Test]
        public void Validate_Should_HaveErrors_WhenProjectIdIsEmpty()
        {
            var model = new TaskModel
            {
                Name = "Task 1",
                Status = StatusTask.Done,
                Priority = 6,
                Description = "Description 1"
           };
            Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _service.ValidateTaskModel(model);
            });
        }
    }
}