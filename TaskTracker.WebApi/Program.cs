using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TaskTracker.BL.MapProfiles;
using TaskTracker.BL.Services;
using TaskTracker.DAL;
using TaskTracker.DAL.Repository;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddTransient<ITaskService,TasksService>();
builder.Services.AddTransient<ITaskRepository,TaskRepository>();
builder.Services.AddTransient<IProjectService,ProjectService>();
builder.Services.AddTransient<IProjectRepository,ProjectRepository>();
builder.Services.AddDbContext<AppDbContext>();

builder.Services.AddAutoMapper(typeof(MapProfile));

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapControllers();

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    var context = services.GetRequiredService<AppDbContext>();    
    context.Database.Migrate();
}

app.Run();