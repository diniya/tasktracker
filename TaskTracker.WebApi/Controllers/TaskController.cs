﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TaskTracker.BL.Models;
using TaskTracker.BL.Services;
using TaskTracker.DAL.Models;

namespace TaskTracker.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService _tasksService;

        public TaskController(ITaskService tasksService)
        {
            _tasksService = tasksService;
        }

        /// <summary>
        /// Method returns list of tasks by project 
        /// </summary>
        /// <param name="projectId">Id of project</param>
        /// <param name="filterParameters">Sorting and filtering parameters
        /// Fields of sorting:
        /// * Name
        /// * Priority
        /// * Status
        /// </param>
        /// <returns></returns>
        [HttpPost("by-project/{projectId:guid}")]
        public async Task<IActionResult> GetTaskList([FromRoute] Guid projectId, [FromBody] TaskFilterParameters filterParameters)
        {
            List<TaskModel> tasks = await _tasksService.GetProjectTasks(projectId, filterParameters);
            return Ok(tasks);
        }

        /// <summary>
        /// Method returns task by Id
        /// </summary>
        /// <param name="taskId">Id of task</param>
        /// <returns></returns>
        [HttpGet("{taskId:guid}")]
        public async Task<IActionResult> GetTask([FromRoute] Guid taskId)
        {
            TaskModel findTask = await _tasksService.GetTask(taskId);
            return Ok(findTask);
        }

        /// <summary>
        /// Method creates new task
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateTask([FromBody] TaskModel task)
        {
            var createdId = await _tasksService.Create(task);
            return CreatedAtAction(nameof(GetTask), new { taskId = createdId }, createdId);
        }

        /// <summary>
        /// Method updates task
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="task"></param>
        /// <returns></returns>
        [HttpPut("{taskId:guid}")]
        public async Task<IActionResult> UpdateTask([FromRoute] Guid taskId, [FromBody] TaskModel task)
        {
            await _tasksService.Update(taskId, task);
            return Ok();
        }

        /// <summary>
        /// Method deletes task by Id
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpDelete("{taskId:guid}")]
        public async Task<IActionResult> DeleteTask([FromRoute] Guid taskId)
        {
            await _tasksService.Delete(taskId);
            return NoContent();
        }
    }
}