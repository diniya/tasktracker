﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TaskTracker.BL.Models;
using TaskTracker.BL.Services;
using TaskTracker.DAL.Models;


namespace TaskTracker.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectsService;

        public ProjectController(IProjectService projectsService)
        {
            _projectsService = projectsService;
        }

        /// <summary>
        /// Method returns list of projects 
        /// </summary>
        /// <param name="filterParameters">Sorting and filtering parameters
        /// Fields of sorting:
        /// * Name
        /// * Priority
        /// * Status
        /// * StartDate
        /// * CompletionDate
        /// </param>
        /// <returns></returns>
        [HttpPost("list")]
        public async Task<IActionResult> GetProjectsList([FromBody] ProjectFilterParameters filterParameters)
        {
           List<ProjectTasksModel> tasks = await _projectsService.GetProjects(filterParameters);
            return Ok(tasks);
        }
        
        /// <summary>
        /// Method returns project by Id
        /// </summary>
        /// <param name="projectId">Id of project</param>
        /// <returns></returns>
        [HttpGet("{projectId:guid}")]
        public async Task<IActionResult> GetProject([FromRoute] Guid projectId)
        {
            ProjectTasksModel findTask = await _projectsService.GetProject(projectId);
            return Ok(findTask);
        }
        
        /// <summary>
        /// Method creates new project
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateProject([FromBody] ProjectModel project)
        {
            var createdId = await _projectsService.Create(project);
            return CreatedAtAction(nameof(GetProject), new { projectId = createdId }, createdId);
        }
        
        /// <summary>
        /// Method updates project
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        [HttpPut("{projectId:guid}")]
        public async Task<IActionResult> UpdateProject([FromRoute] Guid projectId,[FromBody]  ProjectModel project)
        {
            await _projectsService.Update(projectId, project);
            return Ok();
        }
        
        /// <summary>
        /// Method deletes project by Id
        /// </summary>
        /// <param name="projectId">ProjectId</param>
        /// <returns></returns>
        [HttpDelete("{projectId:guid}")]
        public async Task<IActionResult> DeleteProject([FromRoute] Guid projectId)
        {
            await _projectsService.Delete(projectId);
            return NoContent();
        }
        
        
    }
}