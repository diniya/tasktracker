﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using TaskTracker.DAL.Entities;

namespace TaskTracker.DAL
{
    public class AppDbContext: DbContext
    {
        public DbSet<ProjectEntity> Projects { get; set; }
        public DbSet<TaskEntity> ProjectTasks { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProjectEntity>().HasKey(x=>x.Id);
            modelBuilder.Entity<ProjectEntity>().Property(x=>x.Name).HasMaxLength(500);
            modelBuilder.Entity<TaskEntity>().Property(x=>x.Name).HasMaxLength(500);
            modelBuilder.Entity<TaskEntity>().Property(x=>x.Description).HasMaxLength(2000);

            modelBuilder.Entity<TaskEntity>().HasKey(x=>x.Id);
            modelBuilder.Entity<ProjectEntity>()
                .HasMany(x => x.Tasks)
                .WithOne(x => x.Project)
                .HasForeignKey(x=>x.ProjectId)
                .OnDelete(DeleteBehavior.Cascade);
        }
        
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();
                var connectionString = configuration.GetConnectionString("DbConnectionString");
                optionsBuilder.UseSqlServer(connectionString);
            }
        }
        
        
    }
}