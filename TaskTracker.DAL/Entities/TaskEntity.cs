﻿using System;

namespace TaskTracker.DAL.Entities
{
    public class TaskEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public StatusTask Status { get; set; }
        public int Priority { get; set; }
        
        public ProjectEntity Project { get; set; }
        public Guid ProjectId { get; set; }
        
    }
}