﻿using System;
using System.Collections.Generic;

namespace TaskTracker.DAL.Entities
{
    public class ProjectEntity
    {
        public Guid Id { get; }
        public string Name { get; set; }
        public StatusProject Status { get; set; }
        public int Priority { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public List<TaskEntity> Tasks { get; set; }
        
    }
}