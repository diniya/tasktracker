﻿namespace TaskTracker.DAL.Entities
{
    public enum StatusProject
    {
        NotStarted = 0,
        Active,
        Completed
    }
}