﻿namespace TaskTracker.DAL.Entities
{
    public enum StatusTask
    {
        ToDo = 0,
        InProgress,
        Done
    }
}