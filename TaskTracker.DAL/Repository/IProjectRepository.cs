﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskTracker.DAL.Entities;
using TaskTracker.DAL.Models;

namespace TaskTracker.DAL.Repository
{
    public interface IProjectRepository
    {
        /// <summary>
        /// Returns list of projects
        /// </summary>
        /// <param name="filterParameters">Sorting and filtering parameters</param>
        /// <returns></returns>
        Task<List<ProjectEntity>> GetProjects(ProjectFilterParameters filterParameters); 
        
        /// <summary>
        /// Returns project by Id
        /// </summary>
        /// <param name="id">Id of project</param>
        /// <returns></returns>
        Task<ProjectEntity> GetProject(Guid id); 
        
        /// <summary>
        /// Creates new project
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<Guid> Create(ProjectEntity entity); 
       
        /// <summary>
        /// Updates project
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task Update(ProjectEntity entity); 
        
        /// <summary>
        /// Deletes project by Id
        /// </summary>
        /// <param name="id">ProjectId</param>
        /// <returns></returns>
        Task Delete(Guid id); 

    }
}