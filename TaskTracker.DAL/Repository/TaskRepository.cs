﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TaskTracker.DAL.Entities;
using TaskTracker.DAL.Models;

namespace TaskTracker.DAL.Repository
{
    public class TaskRepository : ITaskRepository
    {
        private readonly AppDbContext _dbContext;

        public TaskRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<TaskEntity>> GetProjectTasks(Guid projectId, TaskFilterParameters filterParameters)
        {
            var tasksQuery = _dbContext.ProjectTasks
                .Where(x => x.ProjectId == projectId)
                .AsQueryable();
            if (filterParameters.Status.HasValue)
            {
                tasksQuery = tasksQuery
                    .Where(x => x.Status == filterParameters.Status.Value);
            }

            if (filterParameters.Priority.HasValue)
            {
                tasksQuery = tasksQuery
                    .Where(x => x.Priority == filterParameters.Priority.Value);
            }

            if (!string.IsNullOrEmpty(filterParameters.Name))
            {
                tasksQuery = tasksQuery
                    .Where(x => x.Name.Contains(filterParameters.Name));
            }

            tasksQuery = GetOrderQuery(tasksQuery, filterParameters);
            
            return await tasksQuery.ToListAsync();
        }

        public async Task<TaskEntity> GetTask(Guid taskId)
        {
            return await _dbContext.ProjectTasks
                .Where(x => x.Id == taskId)
                .FirstOrDefaultAsync();
        }

        public async Task<Guid> Create(TaskEntity entity)
        {
            _dbContext.ProjectTasks.Add(entity);
            await _dbContext.SaveChangesAsync();
            return entity.Id;
        }

        public async Task Update(TaskEntity entity)
        {
            _dbContext.ProjectTasks.Update(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            TaskEntity findTask = await _dbContext.ProjectTasks
                .FirstOrDefaultAsync(x => x.Id == id);
            if (findTask == null)
            {
                return;
            }

            _dbContext.ProjectTasks.Remove(findTask);
            await _dbContext.SaveChangesAsync();
        }

        private IQueryable<TaskEntity> GetOrderQuery(IQueryable<TaskEntity> tasksQuery,
            TaskFilterParameters filterParameters)
        {
            if (!string.IsNullOrEmpty(filterParameters.SortField))
            {
                var expression = ResolveSortingExpression(filterParameters.SortField);
                return filterParameters.SortType
                        ? tasksQuery.OrderByDescending(expression)
                        : tasksQuery.OrderBy(expression);
            }

            return tasksQuery;
        }

        private Expression<Func<TaskEntity, object>> ResolveSortingExpression(
            string sortColumn)
        {
            return sortColumn.ToLowerInvariant() switch
            {
                "name" => x => x.Name,
                "priority" => x => x.Priority,
                "status" => x => x.Status,
                _ => x => x.Id
            };
        }
    }
}