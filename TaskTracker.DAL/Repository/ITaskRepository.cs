﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskTracker.DAL.Entities;
using TaskTracker.DAL.Models;

namespace TaskTracker.DAL.Repository
{
    public interface ITaskRepository
    {
        /// <summary>
        /// Returns list of tasks by project 
        /// </summary>
        /// <param name="projectId">Id of project</param>
        /// <param name="filterParameters">Sorting and filtering parameters</param>
        /// <returns></returns>
        Task<List<TaskEntity>> GetProjectTasks(Guid projectId, TaskFilterParameters filterParameters);
        
        /// <summary>
        /// Returns task by Id
        /// </summary>
        /// <param name="taskId">Id of task</param>
        /// <returns></returns>
        Task<TaskEntity> GetTask(Guid taskId);
        
        /// <summary>
        /// Creates new task
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<Guid> Create(TaskEntity entity);
        
        /// <summary>
        /// Updates task
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task Update(TaskEntity entity);
        
        /// <summary>
        /// Deletes task by Id
        /// </summary>
        /// <param name="id">Id of task</param>
        /// <returns></returns>
        Task Delete(Guid id);
        
    }
}