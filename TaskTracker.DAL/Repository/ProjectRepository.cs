﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TaskTracker.DAL.Entities;
using TaskTracker.DAL.Models;

namespace TaskTracker.DAL.Repository
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly AppDbContext _dbContext;

        public ProjectRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<ProjectEntity>> GetProjects(ProjectFilterParameters filterParameters)
        {
            var projectsQuery =  _dbContext.Projects.AsQueryable();
            if (filterParameters.Status.HasValue)
            {
                projectsQuery = projectsQuery
                    .Where(x => x.Status == filterParameters.Status.Value);
            }
            if (filterParameters.Priority.HasValue)
            {
                projectsQuery = projectsQuery
                    .Where(x => x.Priority == filterParameters.Priority.Value);
            }
            if (!string.IsNullOrEmpty(filterParameters.Name))
            {
                projectsQuery = projectsQuery
                    .Where(x => x.Name.Contains(filterParameters.Name));
            }
            if (filterParameters.CompletionDateFrom.HasValue)
            {
                projectsQuery = projectsQuery
                    .Where(x => x.CompletionDate >= filterParameters.CompletionDateFrom.Value);
            }
            if (filterParameters.CompletionDateTo.HasValue)
            {
                projectsQuery = projectsQuery
                    .Where(x => x.CompletionDate <= filterParameters.CompletionDateTo.Value);
            }
            if (filterParameters.StartDateFrom.HasValue)
            {
                projectsQuery = projectsQuery
                    .Where(x => x.StartDate >= filterParameters.StartDateFrom.Value);
            }
            if (filterParameters.StartDateTo.HasValue)
            {
                projectsQuery = projectsQuery
                    .Where(x => x.StartDate <= filterParameters.StartDateTo.Value);
            }
            
            projectsQuery = GetOrderQuery(projectsQuery, filterParameters);
            
            return await projectsQuery.ToListAsync();
        }
        
        public async Task<ProjectEntity> GetProject(Guid id)
        {
            return await _dbContext.Projects
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Guid> Create(ProjectEntity model)
        {
            _dbContext.Projects.Add(model);
            await _dbContext.SaveChangesAsync();
            return model.Id;
        }

        public async Task Update(ProjectEntity entity)
        {
            _dbContext.Projects.Update(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            ProjectEntity findProject = await _dbContext.Projects
                .FirstOrDefaultAsync(x => x.Id == id);
            if (findProject == null)
            {
                return;
            }

            _dbContext.Projects.Remove(findProject);
            await _dbContext.SaveChangesAsync();
        }
        
        
        private IQueryable<ProjectEntity> GetOrderQuery(IQueryable<ProjectEntity> projectsQuery, ProjectFilterParameters filterParameters)
        {
            if (!string.IsNullOrEmpty(filterParameters.SortField))
            {
                var expression = ResolveSortingExpression(filterParameters.SortField);
                return filterParameters.SortType
                        ? projectsQuery.OrderByDescending(expression)
                        : projectsQuery.OrderBy(expression);
              
            }

            return projectsQuery;
        }

        private  Expression<Func<ProjectEntity, object>> ResolveSortingExpression(
            string sortColumn)
        {
            return sortColumn.ToLowerInvariant() switch
            {
                "name" => x => x.Name,
                "priority" => x => x.Priority,
                "status" => x => x.Status,
                "startdate" => x => x.StartDate,
                "completiondate" => x => x.CompletionDate,
                _ => x => x.Id
            };
        }
    }
}