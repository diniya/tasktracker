﻿using System;
using TaskTracker.DAL.Entities;

namespace TaskTracker.DAL.Models
{
    public class ProjectFilterParameters : SortParameters
    {
        public string? Name { get; set; }
        public StatusProject? Status { get; set; }
        public int? Priority { get; set; }
        public DateTime? StartDateFrom { get; set; }
        public DateTime? StartDateTo { get; set; }
        public DateTime? CompletionDateFrom { get; set; }
        public DateTime? CompletionDateTo { get; set; }
    }
}