﻿namespace TaskTracker.DAL.Models
{
    public class SortParameters
    {
        public string SortField { get; set; }
        public bool SortType { get; set; }
    }
}