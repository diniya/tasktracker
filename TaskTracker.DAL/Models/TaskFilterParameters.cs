﻿using TaskTracker.DAL.Entities;

namespace TaskTracker.DAL.Models
{
    public class TaskFilterParameters : SortParameters
    {
        public string? Name { get; set; }
        public StatusTask? Status { get; set; }
        public int? Priority { get; set; }
    }
}