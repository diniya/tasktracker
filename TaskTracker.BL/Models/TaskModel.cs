﻿using System;
using TaskTracker.DAL.Entities;

namespace TaskTracker.BL.Models
{
    public class TaskModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public StatusTask Status { get; set; }
        public int Priority { get; set; }
        
        public Guid ProjectId { get; set; }
    }
}