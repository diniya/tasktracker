﻿using System;
using System.Collections.Generic;
using TaskTracker.DAL.Entities;

namespace TaskTracker.BL.Models
{
    public class ProjectModel
    {
        public string Name { get; set; }
        public StatusProject Status { get; set; }
        public int Priority { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
    }
}