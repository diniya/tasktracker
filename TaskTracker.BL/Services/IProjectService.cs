﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskTracker.BL.Models;
using TaskTracker.DAL.Models;

namespace TaskTracker.BL.Services
{
    public interface IProjectService
    {
        /// <summary>
        /// Returns list of projects
        /// </summary>
        /// <param name="filterParameters">Sorting and filtering parameters</param>
        /// <returns></returns>
        Task<List<ProjectTasksModel>> GetProjects(ProjectFilterParameters filterParameters); 
        
        /// <summary>
        /// Returns project by Id
        /// </summary>
        /// <param name="id">Id of project</param>
        /// <returns></returns>
        Task<ProjectTasksModel> GetProject(Guid id); 
        
        /// <summary>
        /// Creates new project
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Guid> Create(ProjectModel model); 
        
        /// <summary>
        /// Updates project
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        Task Update(Guid projectId,ProjectModel model); 
        
        /// <summary>
        /// Deletes project by Id
        /// </summary>
        /// <param name="id">ProjectId</param>
        /// <returns></returns>
        Task Delete(Guid id); 
    }
}