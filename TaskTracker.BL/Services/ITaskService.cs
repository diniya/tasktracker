﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskTracker.BL.Models;
using TaskTracker.DAL.Models;

namespace TaskTracker.BL.Services
{
    public interface ITaskService
    {
        /// <summary>
        /// Returns list of tasks by project 
        /// </summary>
        /// <param name="projectId">Id of project</param>
        /// <param name="filterParameters">Sorting and filtering parameters</param>
        /// <returns></returns>
        Task<List<TaskModel>> GetProjectTasks(Guid projectId, TaskFilterParameters filterParameters);

        /// <summary>
        /// Returns task by Id
        /// </summary>
        /// <param name="taskId">Id of task</param>
        /// <returns></returns>
        Task<TaskModel> GetTask(Guid taskId);

        /// <summary>
        /// Creates new task
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Guid> Create(TaskModel model);

        /// <summary>
        /// Updates task
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        Task Update(Guid taskId, TaskModel model);

        /// <summary>
        /// Deletes task by Id
        /// </summary>
        /// <param name="id">Id of task</param>
        /// <returns></returns>
        Task Delete(Guid id);
    }
}