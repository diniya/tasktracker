﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using AutoMapper;
using TaskTracker.BL.Models;
using TaskTracker.DAL.Entities;
using TaskTracker.DAL.Models;
using TaskTracker.DAL.Repository;

namespace TaskTracker.BL.Services
{
    public class TasksService : ITaskService
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IMapper _mapper;
        
        public TasksService(ITaskRepository taskRepository, IProjectRepository projectRepository, IMapper mapper)
        {
            _taskRepository = taskRepository;
            _mapper = mapper;
            _projectRepository = projectRepository;
        }

        public async Task<List<TaskModel>> GetProjectTasks(Guid projectId, TaskFilterParameters filterParameters)
        {
            var tasks = await _taskRepository.GetProjectTasks(projectId, filterParameters);
            return _mapper.Map<List<TaskModel>>(tasks);
        }

        public async Task<TaskModel> GetTask(Guid taskId)
        {
            var task = await _taskRepository.GetTask(taskId);
            return _mapper.Map<TaskModel>(task);
        }

        public async Task<Guid> Create(TaskModel model)
        {
            await ValidateTaskModel(model);
            var newTask = _mapper.Map<TaskEntity>(model);
            await _taskRepository.Create(newTask);
            return newTask.Id;
        }

        public async Task Update(Guid taskId, TaskModel model)
        {
            await ValidateTaskModel(model);
            var task = await _taskRepository.GetTask(taskId);
            _mapper.Map(model, task);
            await _taskRepository.Update(task);
        }

        public async Task Delete(Guid id)
        {
            await _taskRepository.Delete(id);
        }
        
        public async Task ValidateTaskModel(TaskModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
            {
                throw new ValidationException("Name is required");
            }

            if (model.Name.Length > 500)
            {
                throw new ValidationException("Max length of Name is 500");
            }
            
            if (model.Description.Length > 2000)
            {
                throw new ValidationException("Max length of Description is 2000");
            }
            
            if (!Enum.IsDefined(model.Status))
            {
                throw new ValidationException("Incorrect value of Status");
            }
            
            if (model.Priority > 5)
            {
                throw new ValidationException("Max value of Priority is 5");
            }
            
            if (model.Priority < 1)
            {
                throw new ValidationException("Min value of Priority is 1");
            }
            
            if (model.ProjectId == Guid.Empty)
            {
                throw new ValidationException("ProjectId is required");
            }

            var project = await _projectRepository.GetProject(model.ProjectId);
            if (project == null)
            {
                throw new ValidationException("Project is not found");
            }
        }
    }
}