﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using AutoMapper;
using TaskTracker.BL.Models;
using TaskTracker.DAL.Entities;
using TaskTracker.DAL.Models;
using TaskTracker.DAL.Repository;

namespace TaskTracker.BL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IMapper _mapper;

        public ProjectService(IProjectRepository projectRepository, IMapper mapper)
        {
            _projectRepository = projectRepository;
            _mapper = mapper;
        }

        public async Task<List<ProjectTasksModel>> GetProjects(ProjectFilterParameters filterParameters)
        {
            var projects = await _projectRepository.GetProjects(filterParameters);
            return _mapper.Map<List<ProjectTasksModel>>(projects);
        }

        public async Task<ProjectTasksModel> GetProject(Guid id)
        {
            var project = await _projectRepository.GetProject(id);
            return _mapper.Map<ProjectTasksModel>(project);
        }

        public async Task<Guid> Create(ProjectModel model)
        {
            ValidateProjectModel(model);
            var newProject = _mapper.Map<ProjectEntity>(model);
            await _projectRepository.Create(newProject);
            return newProject.Id;
        }

        public async Task Update(Guid projectId, ProjectModel model)
        {
            ValidateProjectModel(model);
            var project = await _projectRepository.GetProject(projectId);
            _mapper.Map(model, project);
            await _projectRepository.Update(project);
        }

        public static void ValidateProjectModel(ProjectModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
            {
                throw new ValidationException("Name is required");
            }

            if (model.Name.Length > 500)
            {
                throw new ValidationException("Max length of Name is 500");
            }
            
            if (!Enum.IsDefined(model.Status))
            {
                throw new ValidationException("Incorrect value of Status");
            }
            
            if (model.Priority > 5)
            {
                throw new ValidationException("Max value of Priority is 5");
            }
            
            if (model.Priority < 1)
            {
                throw new ValidationException("Min value of Priority is 1");
            }
        }

        public async Task Delete(Guid id)
        {
            await _projectRepository.Delete(id);
        }
    }
}