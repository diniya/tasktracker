﻿using AutoMapper;
using TaskTracker.BL.Models;
using TaskTracker.DAL.Entities;

namespace TaskTracker.BL.MapProfiles
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<TaskEntity, TaskModel>();
            CreateMap<TaskModel, TaskEntity>()
                .ForMember(x => x.Id, o=>o.Ignore());
            CreateMap<ProjectEntity, ProjectModel>();
            CreateMap<ProjectModel, ProjectEntity>()
                .ForMember(x => x.Id, o=>o.Ignore());
            CreateMap<ProjectEntity, ProjectTasksModel>();
            CreateMap<ProjectModel, ProjectTasksModel>()
                .ForMember(x => x.Id, o=>o.Ignore());

        }
    }
}